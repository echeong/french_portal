
$(function () {

$("#exercise-form").submit(function(e) {
    e.preventDefault();
    var form = document.forms[0];
    var selected = form.querySelector('input[name=image-radio]:checked');
    var selectedVal = selected.value.toLowerCase();
    //console.log(selectedVal);
    
    var correctWord = document.querySelector('source').getAttribute("src");
    var wordArray = correctWord.split('/');
    var fullFileName = wordArray[wordArray.length-1];
    var fileName = decodeURI(fullFileName.split('.')[0]);
    //console.log(fileName);
    var response = document.querySelectorAll('.ex_response');
    for (var i=0; i<response.length; i++) {
        response[i].style.display = "none";
    }
    
    if (selectedVal === fileName) {
        //alert("Correct " + selectedVal + " " + fileName);
        document.querySelector('.ex_response_correct').style.display = "inline";
    }
    else {
        //alert("Incorrect " + selectedVal + " " + fileName);  
        document.querySelector('.ex_response_incorrect').style.display = "inline"; 
    }
})


  var loadForm = function() {
    var btn = $(this);
    $.ajax({
      url: btn.attr('data-url'),
      type: 'get',
      dataType: 'json',
      beforeSend: function() {
        $("#modal-phono-quiz").modal("show");
      },
      success: function(data) {
        $("#modal-phono-quiz .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function() {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function(data) {
        if (data.form_is_valid) {
          $("#phonoquiz-table").html(data.html_quiz_list);
          $("#modal-phono-quiz").modal("hide");
        } else {
          $("#modal-phono-quiz .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };

  $(".js-create-phono-quiz").click(loadForm);
  $("#modal-phono-quiz").on("submit", ".js-phonoquiz-create-form", saveForm);

  $("#phonoquiz-table").on("click", ".js-update-phono-quiz", loadForm);
  $("#modal-phono-quiz").on("submit", ".js-phonoquiz-update-form", saveForm);

  $("#phonoquiz-table").on("click", ".js-delete-phono-quiz", loadForm);
  $("#modal-phono-quiz").on("submit", ".js-phonoquiz-delete-form", saveForm);

  $(".js-create-phono-exer").click(loadForm);

  $("#js-syntax-check-ans").click(function() {
    
    $(".result").remove();
    $.each($(".syntaxoption option:selected"), function() {
      var name = "#" + $(this).parent().attr('name') + "-feedback";
      return_value = checkAnswer($(this).val());
    
      $(name).append(return_value);
      $(name).removeClass("invisible");
      $(name).removeClass("correct");
      $(name).removeClass("incorrect");
      $(name).addClass("show");
      if (!return_value.includes("wrong")) {
        $(name).addClass("correct");
      } else {
        $(name).addClass("incorrect");
      }
      console.log(name);
    });

  });
  function checkAnswer (select) {
    var result = select.split('|');
    console.log(result);
    var feedback = '';
    if (result.length == 2) {
      feedback = result[1];
    }
    if (result[0][0] == '*') {
      return '<span class="result right">&#10003; ' + feedback + "</span>";
    }
    return "<span class='result wrong'>&#10007;</span>";
  }
  function checkCorrect (value) {
    return 
  }
/*
  $("#id_word1").change(function () {
    var word1 = $(this).val();

    $.ajax({
      url: '/ajax/validate/word/',
      data: {
        'word': word1
      },
      dataType: 'json',
      success: function(data) {
        if (data.exists) {
          alert("Word is already in system");
        }
      }
    });
  });
*/
});
