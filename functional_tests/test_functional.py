from django.test import LiveServerTestCase
from django.contrib.auth import get_user_model

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import datetime

class NewVisitorTestCase(LiveServerTestCase):
    fixture = ['learnfrench-data.json']

    @classmethod
    def setUpClass(cls):
        super(NewVisitorTestCase, cls).setUpClass()

        cls.browser = webdriver.Firefox()
        cls.browser.implicitly_wait(10)   

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(NewVisitorTestCase, cls).tearDownClass()

    def test_visitor_go_to_home_page(self):

        self.browser.get(self.live_server_url)
        self.assertIn('French Now!', self.browser.title)

        # TODO: fill in brand element

        home_link = self.browser.find_element_by_link_text('Home')
        home_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/')

    def test_visitor_go_to_news_page(self):

        self.browser.get(self.live_server_url)
        news_link = self.browser.find_element_by_link_text('News')
        news_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/news/')

    def test_visitor_go_to_learning_page(self):

        self.browser.get(self.live_server_url)
        quiz_link = self.browser.find_element_by_link_text('Start learning')
        quiz_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/learn/')

    def test_visitor_go_to_resources_page(self):
        self.browser.get(self.live_server_url)
        resources_link = self.browser.find_element_by_link_text('Resources')
        resources_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/resources/')

    def test_visitor_go_to_about_page(self):
        self.browser.get(self.live_server_url)
        about_link = self.browser.find_element_by_link_text('About')
        about_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/about/')

    def test_visitor_go_to_admin_page(self):
        self.browser.get(self.live_server_url)
        admin_link = self.browser.find_element_by_link_text('Admin')
        admin_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/admin/')

    def test_visitor_play_phonology_quiz(self):
        self.browser.get(self.live_server_url + '/learn/')
        h1 = self.browser.find_element_by_tag_name('h1')
        self.assertEqual(h1.text, 'Exercise')
        first_quiz_link = self.browser.find_element_by_class_name('quiz')
        quiz_text = first_quiz_link.text
        first_quiz_link.click()
        self.assertEqual(self.browser.current_url, self.live_server_url + '/' + quiz_text)
        self.fail("Complete the test")    



#---------------------------------------
# Functional

# Support multiple browsers
    # IE 8+
    # Chrome 61+
    # Safari
    # Firefox
# Support phone/tablet devices (screen resolutions)


