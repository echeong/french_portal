from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.forms import inlineformset_factory
from django.http import JsonResponse
from .models import (Resource, News, PhonologyQuiz, PhonologyExercise, 
        PhonologyFrenchWordPair, PhonologyFrenchWord, SyntaxQuiz, 
        SyntaxExercise, SyntaxQuestion)
from .forms import (PhonologyQuizForm, PhonologyExerciseForm, PhonologyFrenchWordPairForm,
        PhonologyExerciseModelForm, PhonologyFrenchWordForm, SyntaxExerciseForm,
        SyntaxQuestionForm)
from random import randint
import re

def index(request):
    return render(request, 'learnfrench/index.html')

class NewsListView(ListView):
    model = News

class NewsCreate(LoginRequiredMixin, CreateView):
    model = News
    fields = ['title', 'slug', 'content', 'post_date', 'image']

class NewsUpdate(LoginRequiredMixin, UpdateView):
    model = News
    fields = '__all__'

class NewsDelete(LoginRequiredMixin, DeleteView):
    model = News
    success_url = reverse_lazy('news')


def news_article(request, slug):
    article = get_object_or_404(News,slug=slug)
    return render(request, 'learnfrench/news_article.html', {'article': article})

# list phonology and syntax quiz in one list
def learn(request):
    phono_quiz_list = PhonologyQuiz.objects.all()
    syntax_quiz_list = SyntaxQuiz.objects.all()
    return render(request, 'learnfrench/learn.html', 
        {'phono_quiz_list': phono_quiz_list, 
         'syntax_quiz_list': syntax_quiz_list,})    

class ResourceListView(ListView):
    model = Resource

class ResourceCreate(LoginRequiredMixin, CreateView):
    model = Resource
    fields = '__all__'
    success_url = reverse_lazy('resource')

class ResourceUpdate(LoginRequiredMixin, UpdateView):
    model = Resource
    fields = '__all__'

class ResourceDelete(LoginRequiredMixin, DeleteView):
    model = Resource
    success_url = reverse_lazy('resource')

def about(request):
    return render(request, 'learnfrench/about.html')

### Phonology quiz #########

def quiz_item(request, quizid):
    quiz_item = get_object_or_404(PhonologyQuiz, id=quizid)
    ex_list = PhonologyExercise.objects.filter(quiz=quiz_item)
    return render(request, 'learnfrench/quiz.html', 
        {'quiz': quiz_item, 'ex_list': ex_list, 'ex_first': ex_list[0]})

def training(request, quizid, exid):
    quiz_item = get_object_or_404(PhonologyQuiz, id=quizid)
    ex_list = PhonologyExercise.objects.filter(quiz=quiz_item)
    ex_item = get_object_or_404(PhonologyExercise, id=exid)
    pair_list = PhonologyFrenchWordPair.objects.filter(exercise=ex_item).order_by('order')
    if (pair_list.count() > 0):
        pair_first = pair_list[0]
    else:
        pair_first = None

    return render(request, 'learnfrench/training.html',
        {'quiz': quiz_item, 'exitem': ex_item, 'ex_list': ex_list, 'pair_first': pair_list[0]})

def training_item(request, quizid, exid, pairid):
    quiz_item = get_object_or_404(PhonologyQuiz, id=quizid)
    ex_list = PhonologyExercise.objects.filter(quiz=quiz_item)
    ex_item = get_object_or_404(PhonologyExercise, id=exid)
    pair_list = PhonologyFrenchWordPair.objects.filter(exercise=ex_item).order_by('order')
    if (pair_list.count() > 0):
        pair_first = pair_list[0]
    else:
        pair_first = None

    pair = get_object_or_404(PhonologyFrenchWordPair, id=pairid)
    word1 = pair.word1
    word2 = pair.word2
    if (pair.get_next_pair().count() > 0):
        pair_next = pair.get_next_pair()[0]
    else:
        pair_next = None

    return render(request, 'learnfrench/training_item.html', 
        {'quiz': quiz_item, 'exitem': ex_item, 'ex_list': ex_list,
        'word1': word1, 'word2': word2, 'pair_next': pair_next, 'pair_first': pair_first })

def exercise_item(request, quizid, exid, pairid):
    quiz_item = get_object_or_404(PhonologyQuiz, id=quizid)
    ex_list = PhonologyExercise.objects.filter(quiz=quiz_item)
    ex_item = get_object_or_404(PhonologyExercise, id=exid)
    pair_list = PhonologyFrenchWordPair.objects.filter(exercise=ex_item).order_by('order')
    if (pair_list.count() > 0):
        pair_first = pair_list[0]
    else:
        pair_first = None

    pair = get_object_or_404(PhonologyFrenchWordPair, id=pairid)
    word1 = pair.word1
    word2 = pair.word2
    if (pair.get_next_pair().count() > 0):
        pair_next = pair.get_next_pair()[0]
    else:
        pair_next = None

    genint = randint(1,2)
    if (genint == 1):
        random_word = word1
    else:
        random_word = word2

    return render(request, 'learnfrench/exercise_item.html', 
        {'quiz': quiz_item, 'exitem': ex_item, 'ex_list': ex_list,
        'word1': word1, 'word2': word2, 'pair_next': pair_next, 
        'pair_item': pair, 'pair_first': pair_first, 
        'random_word': random_word })

def syntaxquiz_item(request, quizid):
    quiz = get_object_or_404(SyntaxQuiz, id=quizid)
    ex_list = SyntaxExercise.objects.filter(quiz=quiz)
    return render(request, 'learnfrench/syntaxquiz_item.html', 
            {'quiz':quiz, 'ex_list': ex_list})

def syntaxexer_item(request, quizid, exid):
    quiz = get_object_or_404(SyntaxQuiz, id=quizid)
    ex_list = SyntaxExercise.objects.filter(quiz=quiz)
    exer = get_object_or_404(SyntaxExercise, id=exid)
    qu_list = SyntaxQuestion.objects.filter(exercise=exer)

    raw_form = generate_form(qu_list)
    context = {'quiz':quiz, 'ex_list': ex_list, 'exer': exer, 'form': raw_form}
    return render(request, 'learnfrench/syntaxexer_item.html', context)
    """
    return render(request, 'learnfrench/syntaxexer_item.html',
            {'quiz':quiz, 'ex_list': ex_list, 'qu_list': qu_list, 'exer': exer})
    """

def generate_form(qu_list):
    raw_form = ""
    qucount = 0
    for qu in qu_list:
        text = qu.text
        matches = re.finditer(r'{.+?}', text)
        start = 0
        count = 0
        raw_form += "<li>"
        for match in matches:
            raw_form += text[start:match.start()]
            # generate form using match
            raw_form += " "
            raw_form += generate_option(match.group(0), qucount, count)
            raw_form += " "
            start = match.end() + 1
            count += 1
        if start < len(text):
            raw_form += text[start: len(text)]
        raw_form += "</li>"
        qucount += 1
    return raw_form

def generate_option(text, qucount, count):
    raw = "<select name='q" + str(qucount) + "option" + str(count) + "' class='syntaxoption'>"
    sub = text[1:len(text)-1]
    options = sub.split(",")
    for option in options:
        option = option.strip()
        clean_option = option
        # check if option is correct answer
        if option.startswith('*'):
            clean_option = clean_option[1:len(clean_option)]
        if '|' in option:
            clean_option = clean_option[:clean_option.find('|')]
        # get feedback
        raw += "<option value='"
        raw += option
        raw += "'>"
        raw += clean_option
        raw += "</option>"
    raw += "</select><span class='feedback invisible'" + "id='q" + str(qucount) + "option" + str(count) + "-feedback'></span>"
    return raw


### Teacher admin portal
class ManageView(LoginRequiredMixin, TemplateView):
    template_name = "learnfrench/teacherportal/index.html"


class PhonologyQuizListView(LoginRequiredMixin, ListView):
    model = PhonologyQuiz
    template_name = "learnfrench/teacherportal/phonologyquiz_list.html"


def save_phonologyquiz_form(request, form, template):
    data = dict()

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            quiz = PhonologyQuiz.objects.all()
            data['html_quiz_list'] = render_to_string(
                    'learnfrench/teacherportal/includes/partial_phonoquiz_list.html',{
                    'phonologyquiz_list': quiz,
                    })

        else:
            data['form_is_valid'] = False
        
    context = {'form': form}
    data['html_form'] = render_to_string(template, context, request=request)
    return JsonResponse(data)


@login_required
def phonologyquiz_create(request):
            
    if request.method == 'POST':
        form = PhonologyQuizForm(request.POST)
    else:
        form = PhonologyQuizForm()
    return save_phonologyquiz_form(request, form, 'learnfrench/teacherportal/includes/partial_phonoquiz_create.html')


@login_required
def phonologyquiz_update(request, pk):
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    if request.method == 'POST':
        form = PhonologyQuizForm(request.POST, instance=quiz)
    else:
        form = PhonologyQuizForm(instance=quiz)
    return save_phonologyquiz_form(request, form, 'learnfrench/teacherportal/includes/partial_phonoquiz_update.html')


@login_required
def phonologyquiz_delete(request, pk):
    data = dict()
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    if request.method == 'POST':
        quiz.delete()
        data['form_is_valid'] = True
        quiz_list = PhonologyQuiz.objects.all()
        data['html_quiz_list'] = render_to_string(
                'learnfrench/teacherportal/includes/partial_phonoquiz_list.html', {
                    'phonologyquiz_list': quiz_list,
                    })
    else: 
        context = {'quiz': quiz}
        data['html_form'] = render_to_string(
                'learnfrench/teacherportal/includes/partial_phonoquiz_delete.html',
                context,
                request=request
                )
    return JsonResponse(data)

@login_required
def phonologyexer_list(request, pk):
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    exer_list = PhonologyExercise.objects.filter(quiz=quiz)
    return render(request, 'learnfrench/teacherportal/phonologyexer_list.html', {'exer_list':exer_list, 'quiz':quiz})

@login_required
def phonologyexer_create(request, pk):
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    if request.method == 'POST':
        form = PhonologyExerciseForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            level = form.cleaned_data['level']
            exer = PhonologyExercise(title=title, level=level, quiz=quiz)
            exer.save()
            # process input
            return HttpResponseRedirect(reverse('phonoexer-list', args=[pk]))
    else:
        form = PhonologyExerciseForm()
    return render(request, 'learnfrench/teacherportal/phonologyexer_create.html', 
            {'quiz':quiz, 'form':form})

@login_required
def phonologyexer_edit(request, expk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    
    if request.method == 'POST':
        form = PhonologyExerciseModelForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            level = form.cleaned_data['level']
            exer_new = PhonologyExercise(title=title, level=level, quiz=exer.quiz)
            exer_new.save()
            return HttpResponseRedirect(reverse('phonoexer-list', args=[exer.quiz.pk]))
    else:
        form = PhonologyExerciseModelForm(instance=exer)
    return render(request, 'learnfrench/teacherportal/phonologyexer_edit.html', 
            {'exer': exer, 'form':form})

@login_required
def phonologyexer_delete(request, expk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    if request.method == 'POST':
        id = request.POST.get('exerid')
        item = get_object_or_404(PhonologyExercise, pk=id)
        item.delete()
        return HttpResponseRedirect(reverse('phonoexer-list', args=[exer.quiz.pk]))
    return render(request, 'learnfrench/teacherportal/phonologyexer_delete.html', 
            {'exer': exer})

"""
class PhonologyExerWordPairCreate(LoginRequiredMixin, CreateView):
    model = PhonologyFrenchWordPair
    fields = '__all__'
    template_name = 'learnfrench/teacherportal/phonologyexer_wordpair_create.html'
"""

@login_required
def phonologyexerwordpair_create(request, expk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    if request.method == 'POST':
        form = PhonologyFrenchWordPairForm(request.POST)
        if form.is_valid():
            word1 = form.cleaned_data['word1']
            word2 = form.cleaned_data['word2']
            pair = PhonologyFrenchWordPair(word1=word1, word2=word2, exercise=exer, order=1)
            pair.save()
            return redirect('phonoexer-list', pk=exer.quiz.pk)
    else:
        form = PhonologyFrenchWordPairForm()
    return render(request, 'learnfrench/teacherportal/phonologyexer_wordpair_create.html', 
            {'exer':exer, 'form':form})


@login_required
def phonologyexerwordpair_delete(request, pairpk):
    pair = get_object_or_404(PhonologyFrenchWordPair, pk=pairpk)
    pair.delete()
    return redirect('phonoexer-list', pk=pair.exercise.quiz.id)

@login_required
def phonologyword_create(request, expk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    if request.method == 'POST':
        form = PhonologyFrenchWordForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('phonoexer-wordpair-create', expk=exer.pk)
    else:
        form = PhonologyFrenchWordForm()
    return render(request, 'learnfrench/teacherportal/phonologyexer_word_create.html', 
            {'exer':exer, 'form':form})

@login_required
def phonologyword_edit(request, expk, wordpk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    word = get_object_or_404(PhonologyFrenchWord, pk=wordpk)
    if request.method == 'POST':
        form = PhonologyFrenchWordForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('phonoexer-list', pk=exer.quiz.id)
    else:
        form = PhonologyFrenchWordForm(instance=word)
    return render(request, 'learnfrench/teacherportal/phonologyexer_word_edit.html',
            {'exer': exer, 'form':form})
"""
@login_required
def phonologyexer_wordpair_create(request, pk, expk):
    exer = get_object_or_404(PhonologyExercise, pk=expk)
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    PhonoExerInlineFormSet = inlineformset_factory(PhonologyExercise, 
    form = PhonologyExerciseForm(instance=quiz)
    context = {'form': form}
    html_form = render_to_string(
            'learnfrench/teacherportal/includes/partial_phonoexer_create.html',
            context,
            request=request
            )
    return JsonResponse({'html_form':html_form})
    data = dict()
    quiz = get_object_or_404(PhonologyQuiz, pk=pk)
    if request.method == 'POST':
        form = PhonologyExerciseForm(request.POST)
        if form.is_valid():
            exer = form.save(commit=False)
            exer.quiz = quiz
            exer.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    else:

    form = PhonologyExerciseForm()
    context = 
"""

class SyntaxQuizListView(LoginRequiredMixin, ListView):
    model = SyntaxQuiz
    template_name = "learnfrench/teacherportal/syntaxquiz_list.html"

class SyntaxQuizCreateView(LoginRequiredMixin, CreateView):
    model = SyntaxQuiz
    fields = '__all__'
    template_name = "learnfrench/teacherportal/syntaxquiz_create.html"
    success_url = reverse_lazy('manage-syntaxquiz')

class SyntaxQuizUpdateView(LoginRequiredMixin, UpdateView):
    model = SyntaxQuiz
    fields = '__all__'
    template_name = "learnfrench/teacherportal/syntaxquiz_create.html"
    success_url = reverse_lazy('manage-syntaxquiz')

class SyntaxQuizDeleteView(LoginRequiredMixin, DeleteView):
    model = SyntaxQuiz
    template_name = "learnfrench/teacherportal/syntaxquiz_delete.html"
    success_url = reverse_lazy('manage-syntaxquiz')

@login_required
def syntaxexer_create(request, pk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    if request.method == 'POST':
        form = SyntaxExerciseForm(request.POST)
        if form.is_valid():
            new_exer = form.save(commit=False)
            new_exer.quiz = quiz
            new_exer.save()
            return redirect("syntaxexer-list", pk=pk)
    else:
        form = SyntaxExerciseForm()
    return render(request, "learnfrench/teacherportal/syntaxexer_create.html", 
            {"form": form, "quiz":quiz})

@login_required
def syntaxexer_list(request, pk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    exer_list = SyntaxExercise.objects.filter(quiz=quiz)
    return render(request, "learnfrench/teacherportal/syntaxexer_list.html",
            {"quiz":quiz, "exer_list":exer_list})

@login_required
def syntaxexer_edit(request, pk, expk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    exer = get_object_or_404(SyntaxExercise, pk=expk)
    if request.method == 'POST':
        form = SyntaxExerciseForm(request.POST)
        if form.is_valid():
            new_exer = form.save(commit=False)
            new_exer.quiz = quiz
            new_exer.save()
            return redirect("syntaxexer-list", pk=pk)
    else:
        form = SyntaxExerciseForm(instance=exer)
    return render(request, "learnfrench/teacherportal/syntaxexer_create.html", 
            {"form": form, "quiz":quiz})

@login_required
def syntaxexer_delete(request, pk, expk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    exer = get_object_or_404(SyntaxExercise, pk=expk)
    exer.delete()
    return redirect("syntaxexer-list", pk=pk)

@login_required
def syntaxques_create(request, pk, expk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    exer = get_object_or_404(SyntaxExercise, pk=expk)
    if request.method == 'POST':
        form = SyntaxQuestionForm(request.POST)
        if form.is_valid():
            new_ques = form.save(commit=False)
            new_ques.exercise = exer
            new_ques.save()
            return redirect("syntaxexer-list", pk=pk)
    else:
        form = SyntaxQuestionForm()
    return render(request, "learnfrench/teacherportal/syntaxques_create.html",
            {"form": form, "exer":exer, "quiz":quiz})

@login_required
def syntaxques_edit(request, pk, expk, qupk):
    quiz = get_object_or_404(SyntaxQuiz, pk=pk)
    exer = get_object_or_404(SyntaxExercise, pk=expk)
    ques = get_object_or_404(SyntaxQuestion, pk=qupk)
    if request.method == 'POST':
        form = SyntaxQuestionForm(request.POST, instance=ques)
        if form.is_valid():
            form.save()
            #new_text = form.cleaned_data['text']
            #ques.text = new_text
            return redirect("syntaxexer-list", pk=pk)
    else:
        form = SyntaxQuestionForm(instance=ques)
    return render(request, "learnfrench/teacherportal/syntaxques_edit.html",
            {"form": form, "exer":exer, "quiz":quiz, "ques": ques})

@login_required
def syntaxques_delete(request, pk, expk, qupk):
    ques = get_object_or_404(SyntaxQuestion, pk=qupk)
    ques.delete()
    return redirect("syntaxexer-list", pk=pk)
