from django.test import TestCase

from django.core.urlresolvers import reverse

class HomepageViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'learnfrench/index.html')

class NewsPageViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/news/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('news'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('news'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'learnfrench/news.html')

class LearningPageViewTest(TestCase): 

    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/learn/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('learn'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('learn'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'learnfrench/learn.html')

class ResourcesPageViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/resources/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('resources'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('resources'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'learnfrench/resources.html')

class AboutPageViewTest(TestCase):

    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/about/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('about'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('about'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'learnfrench/about.html')
