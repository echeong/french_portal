from django.apps import AppConfig


class LearnfrenchConfig(AppConfig):
    name = 'learnfrench'
