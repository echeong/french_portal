# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-13 08:54
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('learnfrench', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='post_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 13, 8, 54, 43, 224386, tzinfo=utc)),
        ),
    ]
