# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2018-02-28 03:17
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('learnfrench', '0005_auto_20180227_0341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='image/news'),
        ),
        migrations.AlterField(
            model_name='news',
            name='post_date',
            field=models.DateField(default=datetime.datetime(2018, 2, 28, 3, 17, 51, 525109, tzinfo=utc)),
        ),
    ]
