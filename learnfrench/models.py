from django.db import models
from django.urls import reverse
from django.utils import timezone

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField 
#from learnfrench.views import news_article


class Resource(models.Model):
    name = models.CharField(max_length=200)
    url = models.URLField()
    description = RichTextField()

    def __str__(self):
        return self.name


class News(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, allow_unicode=True)
    content = RichTextUploadingField()
    post_date = models.DateField(default=timezone.now())
    image = models.ImageField(upload_to='image/news', null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article', args=[self.slug])

    class Meta:
        verbose_name_plural = 'News'
        ordering = ['-post_date']


########## Models for Phonology training exercises ############

class PhonologyQuiz(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('quiz', args=[str(self.id)])

    def get_admin_url(self):
        return reverse('phonologyquiz-update', args=[str(self.id)])

    class Meta:
        verbose_name_plural = "Phonology quizzes"

class PhonologyExercise(models.Model):
    quiz = models.ForeignKey(PhonologyQuiz, blank=True, null=True)
    LEVEL_CHOICES = (
        ('In', 'Initiales'),
        ('Me', 'Médianes'),
        ('Fi', 'Finales'),
    )
    title = models.CharField(max_length=200)
    level = models.CharField(max_length=2, choices=LEVEL_CHOICES, default='In')

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('training', args=[str(self.quiz.id), str(self.id)])

class PhonologyFrenchWordPair(models.Model):
    word1 = models.ForeignKey('PhonologyFrenchWord', related_name='word1')
    word2 = models.ForeignKey('PhonologyFrenchWord', related_name='word2')
    exercise = models.ForeignKey(PhonologyExercise)
    order = models.IntegerField()

    def __str__(self):
        return self.word1.word + ' / ' + self.word2.word

    def get_next_pair(self):
        return PhonologyFrenchWordPair.objects.filter(exercise=self.exercise, order=self.order+1)

    def get_absolute_url(self):
        return reverse('training_item', args=[str(self.exercise.quiz.id), 
            str(self.exercise.id), str(self.id)])

    def get_next_ex_absolute_url(self):
        return reverse('exercise_item', args=[str(self.exercise.quiz.id), 
            str(self.exercise.id), str(self.id)])        

class PhonologyFrenchWord(models.Model):
    word = models.CharField(max_length=200)
    sound_file_mp3 = models.FileField(null=True, blank=True, upload_to='sound/learnfrench')
    sound_file_ogg = models.FileField(null=True, blank=True, upload_to='sound/learnfrench')
    image_file = models.FileField(null=True, blank=True, upload_to='image/learnfrench')

    def __str__(self):
        return self.word


########### Models for Syntax training exercises #############3

class SyntaxQuiz(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('syntaxquiz', args=[str(self.id)])

    class Meta:
        verbose_name_plural = "Syntax quizzes"


class SyntaxExercise(models.Model):
    name = models.CharField(max_length=200)
    quiz = models.ForeignKey(SyntaxQuiz, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('syntaxexer', quizid=quiz.id, exid=self.id)

    class Meta:
        ordering = ['name']

class SyntaxQuestion(models.Model):
    text = models.TextField()
    exercise = models.ForeignKey(SyntaxExercise)

    def __str__(self):
        return self.text


"""
class SyntaxOptionSet(models.Model):
    question = models.ForeignKey(SyntaxQuestion, on_delete=models.SET_NULL, null=True)
    order = models.IntegerField()

    def __str__(self):
        return "{" + str(self.order) + "}"

class SyntaxOption(models.Model):
    option_set = models.ForeignKey(SyntaxOptionSet, on_delete=models.SET_NULL, null=True)
    choice_text = models.CharField(max_length=200)
    correct = models.BooleanField()
    feedback = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.choice_text

    # need to add validation code. only one correct answer for each question

"""
########### General Question Models ##############
"""
class Question(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.SET_NULL, null=True)
    question_text = models.CharField(max_length=200, blank=True, null=True)
    audiofile = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.SET_NULL, null=True)
    choice_text = models.CharField(max_length=200)

    def __str__(self):
        return self.choice_text
"""
