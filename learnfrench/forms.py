from django import forms
from .models import (PhonologyQuiz, PhonologyExercise, PhonologyFrenchWordPair, 
        PhonologyFrenchWord, SyntaxExercise, SyntaxQuestion)


class PhonologyQuizForm(forms.ModelForm):
    class Meta:
        model = PhonologyQuiz
        fields = '__all__'

class PhonologyExerciseModelForm(forms.ModelForm):
    class Meta:
        model = PhonologyExercise
        fields = ('title', 'level',)

"""
class PhonologyFrenchWordPairForm(forms.ModelForm):
    class Meta:
        model = PhonologyFrenchWordPair
        fields = '__all__'
"""

class PhonologyExerciseForm(forms.Form):
    title = forms.CharField(max_length=200)
    level = forms.ChoiceField(choices=PhonologyExercise.LEVEL_CHOICES)

class PhonologyFrenchWordPairForm(forms.ModelForm):
    class Meta:
        model = PhonologyFrenchWordPair
        fields = ('word1', 'word2')


class PhonologyFrenchWordForm(forms.ModelForm):
    class Meta:
        model = PhonologyFrenchWord
        fields = '__all__'

class SyntaxExerciseForm(forms.ModelForm):
    class Meta:
        model = SyntaxExercise
        fields = ('name',)

class SyntaxQuestionForm(forms.ModelForm):
    class Meta:
        model = SyntaxQuestion
        fields = ('text',)

"""
class PhonologyFrenchWordPairForm(forms.Form):
    word1 = forms.CharField(max_length=200)
    word1_image = forms.ImageField()
    word1_sound_file_mp3 = forms.FileField()
    word1_sound_file_ogg = forms.FileField()
    word2 = forms.CharField(max_length=200)
    word2_image = forms.ImageField()
    word2_sound_file_mp3 = forms.FileField()
    word2_sound_file_ogg = forms.FileField()
"""
"""
class PhonologyExerciseAddPairForm(forms.Form):
    word1 = forms.CharField(max_length=200)
    word2 = forms.CharField(max_length=200)
"""
    
