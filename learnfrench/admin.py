from django.contrib import admin

from learnfrench.models import (Resource, News, PhonologyQuiz, PhonologyExercise, 
    PhonologyFrenchWordPair, PhonologyFrenchWord, SyntaxQuiz, SyntaxExercise, SyntaxQuestion)


admin.site.register(Resource)

class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'post_date')

admin.site.register(News, NewsAdmin)


class PhonologyExerciseInline(admin.StackedInline):
    model = PhonologyExercise

class PhonologyQuizAdmin(admin.ModelAdmin):
    inlines = [
        PhonologyExerciseInline,
    ]

class PhonologyFrenchWordPairInline(admin.TabularInline):
    model = PhonologyFrenchWordPair

class PhonologyExerciseAdmin(admin.ModelAdmin):
    inlines = [
        PhonologyFrenchWordPairInline,
    ]
    list_display = ('quiz', 'title', 'level')
    list_display_links = ('title',)

class PhonologyFrenchWordAdmin(admin.ModelAdmin):
    list_display = ('word', 'sound_file_mp3', 'sound_file_ogg', 'image_file')

class SyntaxExerciseAdmin(admin.ModelAdmin):
    list_display = ('quiz', 'name')
    list_display_links = ('name',)

admin.site.register(PhonologyQuiz, PhonologyQuizAdmin)
admin.site.register(PhonologyExercise, PhonologyExerciseAdmin)
admin.site.register(PhonologyFrenchWordPair)
admin.site.register(PhonologyFrenchWord, PhonologyFrenchWordAdmin)

admin.site.register(SyntaxQuiz)
admin.site.register(SyntaxExercise, SyntaxExerciseAdmin)
admin.site.register(SyntaxQuestion)
