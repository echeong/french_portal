from django.conf.urls import url, include
from django.contrib import admin
from learnfrench import views

urlpatterns = [
    
    url(r'^$', views.index, name='index'),
    url(r'^news/add/$', views.NewsCreate.as_view(), name="news-add"),
    url(r'^news/(?P<slug>[-\w\d]+)/update/$', views.NewsUpdate.as_view(), name='news-update'), 
    url(r'^news/(?P<slug>[-\w\d]+)/delete/$', views.NewsDelete.as_view(), name='news-delete'), 
    url(r'^news/(?P<slug>[-\w\d]+)/$', view=views.news_article, name='article'), 
    url(r'^news/$', views.NewsListView.as_view(), name="news"),

    url(r'^resource/add/$', views.ResourceCreate.as_view(), name="resource-add"),
    url(r'^resource/(?P<pk>\d+)/update/$', views.ResourceUpdate.as_view(), name="resource-update"),
    url(r'^resource/(?P<pk>\d+)/delete/$', views.ResourceDelete.as_view(), name="resource-delete"),
    url(r'^resource/$', views.ResourceListView.as_view(), name="resource"),

    url(r'^learn/phonology/(?P<quizid>\d+)/(?P<exid>\d+)/training/(?P<pairid>\d+)/$', view=views.training_item, name='training_item'),
    url(r'^learn/phonology/(?P<quizid>\d+)/(?P<exid>\d+)/training/$', view=views.training, name='training'),
    url(r'^learn/phonology/(?P<quizid>\d+)/(?P<exid>\d+)/exercise/(?P<pairid>\d+)/$', view=views.exercise_item, name='exercise_item'),
    url(r'^learn/phonology/(?P<quizid>\d+)/$', view=views.quiz_item, name='quiz'),

    url(r'^learn/syntaxquiz/(?P<quizid>\d+)/$', view=views.syntaxquiz_item, name='syntaxquiz'),
    url(r'^learn/syntaxquiz/(?P<quizid>\d+)/exercise/(?P<exid>\d+)$', view=views.syntaxexer_item, name='syntaxexer'),
    
    url(r'^learn/$', views.learn, name="learn"),
    url(r'^about/', views.about, name="about"),

    url(r'^manage/$', views.ManageView.as_view(), name='manage'),
    url(r'^manage/phonologyquiz/$', views.PhonologyQuizListView.as_view(), name='manage-phonologyquiz'),
    url(r'^manage/phonologyquiz/create/$', views.phonologyquiz_create, name='phonoquiz-create'),
    url(r'^manage/phonologyquiz/(?P<pk>\d+)/update/$', views.phonologyquiz_update, name='phonoquiz-update'),
    url(r'^manage/phonologyquiz/(?P<pk>\d+)/delete/$', views.phonologyquiz_delete, name='phonoquiz-delete'),
    url(r'^manage/phonologyquiz/(?P<pk>\d+)/exercise/create/$', views.phonologyexer_create, name='phonoexer-create'),
    url(r'^manage/phonologyquiz/(?P<pk>\d+)/exercise/list/$', views.phonologyexer_list, name='phonoexer-list'),
    url(r'^manage/phonologyquiz/exercise/(?P<expk>\d+)/edit/$', views.phonologyexer_edit, name='phonoexer-edit'),
    url(r'^manage/phonologyquiz/exercise/(?P<expk>\d+)/delete/$', views.phonologyexer_delete, name='phonoexer-delete'),
    url(r'^manage/phonologyquiz/exercise/(?P<expk>\d+)/wordpair/create/$', views.phonologyexerwordpair_create, name='phonoexer-wordpair-create'),
    url(r'^manage/phonologyquiz/exercise/wordpair/(?P<pairpk>\d+)/delete/$', views.phonologyexerwordpair_delete, name='phonoexer-wordpair-delete'),
    url(r'^manage/phonologyquiz/exercise/(?P<expk>\d+)/word/create/$', views.phonologyword_create, name='phonoword-create'),
    url(r'^manage/phonologyquiz/exercise/(?P<expk>\d+)/word/(?P<wordpk>\d+)/edit/$', views.phonologyword_edit, name='phonoword-edit'),
    
    url(r'^manage/syntaxquiz/$', views.SyntaxQuizListView.as_view(), name='manage-syntaxquiz'),
    url(r'^manage/syntaxquiz/create$', views.SyntaxQuizCreateView.as_view(), name='syntaxquiz-create'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/update$', views.SyntaxQuizUpdateView.as_view(), name='syntaxquiz-update'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/delete$', views.SyntaxQuizDeleteView.as_view(), name='syntaxquiz-delete'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/create/$', views.syntaxexer_create, name='syntaxexer-create'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/list/$', views.syntaxexer_list, name='syntaxexer-list'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/(?P<expk>\d+)/edit/$', views.syntaxexer_edit, name='syntaxexer-edit'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/(?P<expk>\d+)/delete/$', views.syntaxexer_delete, name='syntaxexer-delete'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/(?P<expk>\d+)/question/create/$', views.syntaxques_create, name='syntaxques-create'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/(?P<expk>\d+)/question/(?P<qupk>\d+)/edit/$', views.syntaxques_edit, name='syntaxques-edit'),
    url(r'^manage/syntaxquiz/(?P<pk>\d+)/exercise/(?P<expk>\d+)/question/(?P<qupk>\d+)/delete/$', views.syntaxques_delete, name='syntaxques-delete'),

]

#Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    url(r'^accounts/', include('django.contrib.auth.urls')),
]
