from django.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser, 
        PermissionsMixin, Group)


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        is_active = extra_fields.pop("is_active", True)

        user = self.model(email=email, is_staff=is_staff, is_active=is_active,
                is_superuser=is_superuser, last_login=now,
                date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        is_staff = extra_fields.pop("is_staff", True)
        return self._create_user(email, password, is_staff, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class AbstractEmailUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
            verbose_name='email address',
            max_length=255,
            unique=True,
            )
    is_staff = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    #is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        abstract = True
    
    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)


class User(AbstractEmailUser):
    class Meta(AbstractEmailUser.Meta):
        swappable = 'AUTH_USER_MODEL'
