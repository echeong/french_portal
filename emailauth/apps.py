from django.apps import AppConfig


class EmailauthConfig(AppConfig):
    name = 'emailauth'
